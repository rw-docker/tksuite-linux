FROM ubuntu:16.04
LABEL maintainer "Ralf Weinbrecher <mail@ralfweinbrecher.de>"


RUN apt-get update && apt-get install -y wget gnupg --no-install-recommends
RUN wget --no-check-certificate https://apt.agfeo.de/agfeo.pem
RUN apt-key add agfeo.pem
RUN echo 'deb http://apt.agfeo.de/xenial/tksuite/stable_1_10/ xenial main' >> /etc/apt/sources.list

# COPY agfeo.list /etc/apt/sources.list.d/

RUN apt-get update && apt-get install -y tksuite-es

ENV LANG de-DE

ENTRYPOINT [ "/usr/bin/ctimon" ]
